//header croll
let nav = document.querySelector(".navbar") ;
window.onscroll = function () {
    if(document.documentElement.scrollTop > 40 || document.documentElement.scrollTop == 0){
        nav.classList.add("header-scrolled");
    }
    else{
        nav.classList.remove("header-scrolled");
    }
}